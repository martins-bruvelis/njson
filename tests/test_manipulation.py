import unittest

from njson import NestedJson

class TestManipulation(unittest.TestCase):
    '''This class provides test case for manipulating `NestedJson` data.'''

    def test_del_0(self):
        tnd = NestedJson(
            [
                'v0',
                [1],
                {'l1k0': 'v2'},
                [[[[]]]]
            ]
        )
        del tnd[0]
        self.assertEqual(
            tnd.data,
            [
                [1],
                {'l1k0': 'v2'},
                [[[[]]]]
            ]
        )

    def test_del_1_0(self):
        tnd = NestedJson(
            [
                'v0',
                [1],
                {'l1k0': 'v2'},
                [[[[]]]]
            ]
        )
        del tnd[1,0]
        self.assertEqual(
            tnd.data,
            [
                'v0',
                [],
                {'l1k0': 'v2'},
                [[[[]]]]
            ]
        )

    def test_del_2_l1k0(self):
        tnd = NestedJson(
            [
                'v0',
                [1],
                {'l1k0': 'v2'},
                [[[[]]]]
            ]
        )
        del tnd[2,'l1k0']
        self.assertEqual(
            tnd.data,
            [
                'v0',
                [1],
                {},
                [[[[]]]]
            ]
        )

    def test_del_3_0_0_0(self):
        tnd = NestedJson(
            [
                'v0',
                [1],
                {'l1k0': 'v2'},
                [[[[]]]]
            ]
        )
        del tnd[3,0,0,0]
        self.assertEqual(
            tnd.data,
            [
                'v0',
                [1],
                {'l1k0': 'v2'},
                [[[]]]
            ]
        )

    def test_remove_0(self):
        tnd = NestedJson(
            [
                'v0',
                [1],
                {'l1k0': 'v2'},
                [[[[]]]]
            ]
        )
        self.assertEqual(
            tnd.remove(0).data,
            [
                [1],
                {'l1k0': 'v2'},
                [[[[]]]]
            ]
        )

    def test_remove_1_0(self):
        tnd = NestedJson(
            [
                'v0',
                [1],
                {'l1k0': 'v2'},
                [[[[]]]]
            ]
        )
        self.assertEqual(
            tnd.remove(1,0).data,
            [
                'v0',
                [],
                {'l1k0': 'v2'},
                [[[[]]]]
            ]
        )

    def test_remove_2_l1k0(self):
        tnd = NestedJson(
            [
                'v0',
                [1],
                {'l1k0': 'v2'},
                [[[[]]]]
            ]
        )
        self.assertEqual(
            tnd.remove(2,'l1k0').data,
            [
                'v0',
                [1],
                {},
                [[[[]]]]
            ]
        )

    def test_remove_3_0_0_0(self):
        tnd = NestedJson(
            [
                'v0',
                [1],
                {'l1k0': 'v2'},
                [[[[]]]]
            ]
        )
        self.assertEqual(
            tnd.remove(3,0,0,0).data,
            [
                'v0',
                [1],
                {'l1k0': 'v2'},
                [[[]]]
            ]
        )

    def test_clear_list(self):
        tnd = NestedJson(
            [
                'v0',
                [1],
                {'l1k0': 'v2'},
                [[[[]]]]
            ]
        )
        self.assertEqual(
            tnd.clear().data,
            []
        )

    def test_clear_dict(self):
        tnd = NestedJson(
            {'l1k0': 'v0', 'l1k0': ['v1']}
        )
        self.assertEqual(
            tnd.clear().data,
            {}
        )

    def test_clear_1(self):
        tnd = NestedJson(
            [
                'v0',
                [1],
                {'l1k0': 'v2'},
                [[[[]]]]
            ]
        )
        self.assertEqual(
            tnd.clear(1).data,
            [
                'v0',
                [],
                {'l1k0': 'v2'},
                [[[[]]]]
            ]
        )

    def test_clear_2(self):
        tnd = NestedJson(
            [
                'v0',
                [1],
                {'l1k0': 'v2'},
                [[[[]]]]
            ]
        )
        self.assertEqual(
            tnd.clear(2).data,
            [
                'v0',
                [1],
                {},
                [[[[]]]]
            ]
        )

    def test_clear_3_0_0(self):
        tnd = NestedJson(
            [
                'v0',
                [1],
                {'l1k0': 'v2'},
                [[[[]]]]
            ]
        )
        self.assertEqual(
            tnd.clear(3,0,0).data,
            [
                'v0',
                [1],
                {'l1k0': 'v2'},
                [[[]]]
            ]
        )

    def test_replace_0(self):
        tnd = NestedJson(
            [
                'v0',
                [1],
                {'l1k0': 'v2'},
                [[[[]]]]
            ]
        )
        self.assertEqual(
            tnd.replace('obj', 0).data,
            [
                'obj',
                [1],
                {'l1k0': 'v2'},
                [[[[]]]]
            ]
        )

    def test_replace_1_0(self):
        tnd = NestedJson(
            [
                'v0',
                [1],
                {'l1k0': 'v2'},
                [[[[]]]]
            ]
        )
        self.assertEqual(
            tnd.replace('obj', 1, 0).data,
            [
                'v0',
                ['obj'],
                {'l1k0': 'v2'},
                [[[[]]]]
            ]
        )

    def test_replace_2_l1k0(self):
        tnd = NestedJson(
            [
                'v0',
                [1],
                {'l1k0': 'v2'},
                [[[[]]]]
            ]
        )
        self.assertEqual(
            tnd.replace('obj', 2, 'l1k0').data,
            [
                'v0',
                [1],
                {'l1k0': 'obj'},
                [[[[]]]]
            ]
        )

    def test_replace_3_0_0(self):
        tnd = NestedJson(
            [
                'v0',
                [1],
                {'l1k0': 'v2'},
                [[[[]]]]
            ]
        )
        self.assertEqual(
            tnd.replace('obj', 3, 0, 0).data,
            [
                'v0',
                [1],
                {'l1k0': 'v2'},
                [['obj']]
            ]
        )

    def test_replace_missing_0_0_0(self):
        tnd = NestedJson(
            [
                'v0',
                [1],
                {'l1k0': 'v2'},
                [[[[]]]]
            ]
        )
        self.assertEqual(
            tnd.replace('obj', 0, 0, 0).data,
            [
                'v0',
                [1],
                {'l1k0': 'v2'},
                [[[[]]]]
            ]
        )

    def test_replace_missing_2_0(self):
        tnd = NestedJson(
            [
                'v0',
                [1],
                {'l1k0': 'v2'},
                [[[[]]]]
            ]
        )
        self.assertEqual(
            tnd.replace('obj', 2, 0).data,
            [
                'v0',
                [1],
                {'l1k0': 'v2'},
                [[[[]]]]
            ]
        )

    def test_replace_missing_3_0_1(self):
        tnd = NestedJson(
            [
                'v0',
                [1],
                {'l1k0': 'v2'},
                [[[[]]]]
            ]
        )
        self.assertEqual(
            tnd.replace('obj', 3, 0, 1).data,
            [
                'v0',
                [1],
                {'l1k0': 'v2'},
                [[[[]]]]
            ]
        )

    def test_update_0(self):
        tnd = NestedJson(
            [
                'v0',
                [1],
                {'l1k0': 'v2'},
                [[[[]]]]
            ]
        )
        self.assertEqual(
            tnd.update('obj', 0).data,
            [
                'obj',
                [1],
                {'l1k0': 'v2'},
                [[[[]]]]
            ]
        )

    def test_update_1_0(self):
        tnd = NestedJson(
            [
                'v0',
                [1],
                {'l1k0': 'v2'},
                [[[[]]]]
            ]
        )
        self.assertEqual(
            tnd.update('obj', 1, 0).data,
            [
                'v0',
                ['obj'],
                {'l1k0': 'v2'},
                [[[[]]]]
            ]
        )

    def test_update_2_l1k0(self):
        tnd = NestedJson(
            [
                'v0',
                [1],
                {'l1k0': 'v2'},
                [[[[]]]]
            ]
        )
        self.assertEqual(
            tnd.update('obj', 2, 'l1k0').data,
            [
                'v0',
                [1],
                {'l1k0': 'obj'},
                [[[[]]]]
            ]
        )

    def test_update_3_0_0(self):
        tnd = NestedJson(
            [
                'v0',
                [1],
                {'l1k0': 'v2'},
                [[[[]]]]
            ]
        )
        self.assertEqual(
            tnd.update('obj', 3, 0, 0).data,
            [
                'v0',
                [1],
                {'l1k0': 'v2'},
                [['obj']]
            ]
        )

    def test_update_missing_0_0_0(self):
        tnd = NestedJson(
            [
                'v0',
                [1],
                {'l1k0': 'v2'},
                [[[[]]]]
            ]
        )
        self.assertEqual(
            tnd.update('obj', 0, 0, 0).data,
            [
                [['obj']],
                [1],
                {'l1k0': 'v2'},
                [[[[]]]]
            ]
        )

    def test_update_missing_2_l1k1(self):
        tnd = NestedJson(
            [
                'v0',
                [1],
                {'l1k0': 'v2'},
                [[[[]]]]
            ]
        )
        self.assertEqual(
            tnd.update('obj', 2, 'l1k1').data,
            [
                'v0',
                [1],
                {'l1k0': 'v2', 'l1k1': 'obj'},
                [[[[]]]]
            ]
        )

    def test_update_missing_2_0(self):
        tnd = NestedJson(
            [
                'v0',
                [1],
                {'l1k0': 'v2'},
                [[[[]]]]
            ]
        )
        self.assertEqual(
            tnd.update('obj', 2, 0).data,
            [
                'v0',
                [1],
                {'l1k0': 'v2', 0: 'obj'},
                [[[[]]]]
            ]
        )

    def test_update_missing_3_0_1(self):
        tnd = NestedJson(
            [
                'v0',
                [1],
                {'l1k0': 'v2'},
                [[[[]]]]
            ]
        )
        self.assertEqual(
            tnd.update('obj', 3, 0, 1).data,
            [
                'v0',
                [1],
                {'l1k0': 'v2'},
                [[[[]],'obj']]
            ]
        )

    def test_update_missing_3_0_m5(self):
        tnd = NestedJson(
            [
                'v0',
                [1],
                {'l1k0': 'v2'},
                [[[[]]]]
            ]
        )
        self.assertEqual(
            tnd.update('obj', 3, 0, -5).data,
            [
                'v0',
                [1],
                {'l1k0': 'v2'},
                [['obj',[[]]]]
            ]
        )

    def test_set_default_0(self):
        tnd = NestedJson(
            [
                'v0',
                [1],
                {'l1k0': 'v2'},
                [[[[]]]]
            ]
        )
        self.assertEqual(
            tnd.setdefault('obj', 0).data,
            [
                'v0',
                [1],
                {'l1k0': 'v2'},
                [[[[]]]]
            ]
        )

    def test_set_default_1_0(self):
        tnd = NestedJson(
            [
                'v0',
                [1],
                {'l1k0': 'v2'},
                [[[[]]]]
            ]
        )
        self.assertEqual(
            tnd.setdefault('obj', 1, 0).data,
            [
                'v0',
                [1],
                {'l1k0': 'v2'},
                [[[[]]]]
            ]
        )

    def test_set_default_2_l1k0(self):
        tnd = NestedJson(
            [
                'v0',
                [1],
                {'l1k0': 'v2'},
                [[[[]]]]
            ]
        )
        self.assertEqual(
            tnd.setdefault('obj', 2, 'l1k0').data,
            [
                'v0',
                [1],
                {'l1k0': 'v2'},
                [[[[]]]]
            ]
        )

    def test_set_default_3_0_0(self):
        tnd = NestedJson(
            [
                'v0',
                [1],
                {'l1k0': 'v2'},
                [[[[]]]]
            ]
        )
        self.assertEqual(
            tnd.setdefault('obj', 3, 0, 0).data,
            [
                'v0',
                [1],
                {'l1k0': 'v2'},
                [[[[]]]]
            ]
        )

    def test_set_default_missing_0_0_0(self):
        tnd = NestedJson(
            [
                'v0',
                [1],
                {'l1k0': 'v2'},
                [[[[]]]]
            ]
        )
        self.assertEqual(
            tnd.setdefault('obj', 0, 0, 0).data,
            [
                [['obj']],
                [1],
                {'l1k0': 'v2'},
                [[[[]]]]
            ]
        )

    def test_set_default_missing_2_l1k1(self):
        tnd = NestedJson(
            [
                'v0',
                [1],
                {'l1k0': 'v2'},
                [[[[]]]]
            ]
        )
        self.assertEqual(
            tnd.setdefault('obj', 2, 'l1k1').data,
            [
                'v0',
                [1],
                {'l1k0': 'v2', 'l1k1': 'obj'},
                [[[[]]]]
            ]
        )

    def test_set_default_missing_2_0(self):
        tnd = NestedJson(
            [
                'v0',
                [1],
                {'l1k0': 'v2'},
                [[[[]]]]
            ]
        )
        self.assertEqual(
            tnd.setdefault('obj', 2, 0).data,
            [
                'v0',
                [1],
                {'l1k0': 'v2', 0: 'obj'},
                [[[[]]]]
            ]
        )

    def test_set_default_missing_3_0_1(self):
        tnd = NestedJson(
            [
                'v0',
                [1],
                {'l1k0': 'v2'},
                [[[[]]]]
            ]
        )
        self.assertEqual(
            tnd.setdefault('obj', 3, 0, 1).data,
            [
                'v0',
                [1],
                {'l1k0': 'v2'},
                [[[[]],'obj']]
            ]
        )

    def test_set_default_missing_3_0_m5(self):
        tnd = NestedJson(
            [
                'v0',
                [1],
                {'l1k0': 'v2'},
                [[[[]]]]
            ]
        )
        self.assertEqual(
            tnd.setdefault('obj', 3, 0, -5).data,
            [
                'v0',
                [1],
                {'l1k0': 'v2'},
                [['obj',[[]]]]
            ]
        )

if __name__ == '__main__':
    unittest.main()
