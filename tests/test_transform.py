import unittest

from njson import NestedJson

# nested list
list_nested = [
    'v0',
    [1],
    {'l1k0': 'v2'},
    [[[[]]]]
]
list_nested_as_flat_dict = {
    (0,): 'v0',
    (1, 0): 1,
    (2, 'l1k0'): 'v2',
    (3, 0, 0, 0): [],
}
list_nested_as_data_series = {
    (0, '', '', ''): 'v0',
    (1, 0, '', ''): 1,
    (2, 'l1k0', '', ''): 'v2',
    (3, 0, 0, 0): [],
}
list_nested_as_data_series_bfill = {
    ('', '', '', 0): 'v0',
    ('', '', 1, 0): 1,
    ('', '', 2, 'l1k0'): 'v2',
    (3, 0, 0, 0): [],
}

# nested dictionary
dict_nested = {
    'l0k0': list_nested,
    'l0k1': [list_nested],
    'l0k2': {'l1k0': list_nested}
}
dict_nested_as_flat_dict = {
    ('l0k0', 0): 'v0',
    ('l0k0', 1, 0): 1,
    ('l0k0', 2, 'l1k0'): 'v2',
    ('l0k0', 3, 0, 0, 0): [],
    ('l0k1', 0, 0): 'v0',
    ('l0k1', 0, 1, 0): 1,
    ('l0k1', 0, 2, 'l1k0'): 'v2',
    ('l0k1', 0, 3, 0, 0, 0): [],
    ('l0k2', 'l1k0', 0): 'v0',
    ('l0k2', 'l1k0', 1, 0): 1,
    ('l0k2', 'l1k0', 2, 'l1k0'): 'v2',
    ('l0k2', 'l1k0', 3, 0, 0, 0): [],
}
dict_nested_as_data_series = {
    ('l0k0', 0, '', '', '', ''): 'v0',
    ('l0k0', 1, 0, '', '', ''): 1,
    ('l0k0', 2, 'l1k0', '', '', ''): 'v2',
    ('l0k0', 3, 0, 0, 0, ''): [],
    ('l0k1', 0, 0, '', '', ''): 'v0',
    ('l0k1', 0, 1, 0, '', ''): 1,
    ('l0k1', 0, 2, 'l1k0', '', ''): 'v2',
    ('l0k1', 0, 3, 0, 0, 0): [],
    ('l0k2', 'l1k0', 0, '', '', ''): 'v0',
    ('l0k2', 'l1k0', 1, 0, '', ''): 1,
    ('l0k2', 'l1k0', 2, 'l1k0', '', ''): 'v2',
    ('l0k2', 'l1k0', 3, 0, 0, 0): [],
}
dict_nested_as_data_series_bfill = {
    ('', '', '', '', 'l0k0', 0): 'v0',
    ('', '', '', 'l0k0', 1, 0): 1,
    ('', '', '', 'l0k0', 2, 'l1k0'): 'v2',
    ('', 'l0k0', 3, 0, 0, 0): [],
    ('', '', '', 'l0k1', 0, 0): 'v0',
    ('', '', 'l0k1', 0, 1, 0): 1,
    ('', '', 'l0k1', 0, 2, 'l1k0'): 'v2',
    ('l0k1', 0, 3, 0, 0, 0): [],
    ('', '', '', 'l0k2', 'l1k0', 0): 'v0',
    ('', '', 'l0k2', 'l1k0', 1, 0): 1,
    ('', '', 'l0k2', 'l1k0', 2, 'l1k0'): 'v2',
    ('l0k2', 'l1k0', 3, 0, 0, 0): [],
}

# nested flat-dict
flat_dict_nested = {
    (0,): 'v0',
    (1, 0): 1,
    (2, 'l1k0'): 'v2',
    (3, 0, 0, 0): [
        {
            (0,): 'v0',
            (1, 0): 1,
            (2, 'l1k0'): 'v2',
            (3, 0, 0, 0): [
                {
                (0,): 'v0',
                (1, 0): 1,
                (2, 'l1k0'): 'v2',
                (3, 0, 0, 0): [],
            }
            ],
        }
    ],
}
flat_dict_nested_as_nested_json = [
    'v0',
    [1],
    {'l1k0': 'v2'},
    [[[[
        [
            'v0',
            [1],
            {'l1k0': 'v2'},
            [[[[
                [
                    'v0',
                    [1],
                    {'l1k0': 'v2'},
                    [[[[]]]]
                ]
            ]]]]
        ]
    ]]]]
]
flat_dict_nested_as_flat_dict = {
    (0,): 'v0',
    (1, 0): 1,
    (2, 'l1k0'): 'v2',
    (3, 0, 0, 0, 0, 0): 'v0',
    (3, 0, 0, 0, 0, 1, 0): 1,
    (3, 0, 0, 0, 0, 2, 'l1k0'): 'v2',
    (3, 0, 0, 0, 0, 3, 0, 0, 0, 0, 0): 'v0',
    (3, 0, 0, 0, 0, 3, 0, 0, 0, 0, 1, 0): 1,
    (3, 0, 0, 0, 0, 3, 0, 0, 0, 0, 2, 'l1k0'): 'v2',
    (3, 0, 0, 0, 0, 3, 0, 0, 0, 0, 3, 0, 0, 0): [],
}
flat_dict_nested_as_data_series = {
    (0, '', '', '', '', '', '', '', '', '', '', '', '', ''): 'v0',
    (1, 0, '', '', '', '', '', '', '', '', '', '', '', ''): 1,
    (2, 'l1k0', '', '', '', '', '', '', '', '', '', '', '', ''): 'v2',
    (3, 0, 0, 0, 0, 0, '', '', '', '', '', '', '', ''): 'v0',
    (3, 0, 0, 0, 0, 1, 0, '', '', '', '', '', '', ''): 1,
    (3, 0, 0, 0, 0, 2, 'l1k0', '', '', '', '', '', '', ''): 'v2',
    (3, 0, 0, 0, 0, 3, 0, 0, 0, 0, 0, '', '', ''): 'v0',
    (3, 0, 0, 0, 0, 3, 0, 0, 0, 0, 1, 0, '', ''): 1,
    (3, 0, 0, 0, 0, 3, 0, 0, 0, 0, 2, 'l1k0', '', ''): 'v2',
    (3, 0, 0, 0, 0, 3, 0, 0, 0, 0, 3, 0, 0, 0): [],
}
flat_dict_nested_as_data_series_bfill = {
    ('', '', '', '', '', '', '', '', '', '', '', '', '', 0): 'v0',
    ('', '', '', '', '', '', '', '', '', '', '', '', 1, 0): 1,
    ('', '', '', '', '', '', '', '', '', '', '', '', 2, 'l1k0'): 'v2',
    ('', '', '', '', '', '', '', '', 3, 0, 0, 0, 0, 0): 'v0',
    ('', '', '', '', '', '', '', 3, 0, 0, 0, 0, 1, 0): 1,
    ('', '', '', '', '', '', '', 3, 0, 0, 0, 0, 2, 'l1k0'): 'v2',
    ('', '', '', 3, 0, 0, 0, 0, 3, 0, 0, 0, 0, 0): 'v0',
    ('', '', 3, 0, 0, 0, 0, 3, 0, 0, 0, 0, 1, 0): 1,
    ('', '', 3, 0, 0, 0, 0, 3, 0, 0, 0, 0, 2, 'l1k0'): 'v2',
    (3, 0, 0, 0, 0, 3, 0, 0, 0, 0, 3, 0, 0, 0): [],
}

# nested data series (forward fill)
data_series_nested = {
    (0, '', '', ''): 'v0',
    (1, 0, '', ''): 1,
    (2, 'l1k0', '', ''): 'v2',
    (3, 0, 0, 0): [
        {
            (0, '', '', ''): 'v0',
            (1, 0, '', ''): 1,
            (2, 'l1k0', '', ''): 'v2',
            (3, 0, 0, 0): [
                {
                    (0, '', '', ''): 'v0',
                    (1, 0, '', ''): 1,
                    (2, 'l1k0', '', ''): 'v2',
                    (3, 0, 0, 0): [],
                }
            ],
        }
    ],
}
data_series_nested_as_nested_json = flat_dict_nested_as_nested_json
data_series_nested_as_flat_dict = flat_dict_nested_as_flat_dict
data_series_nested_as_data_series = flat_dict_nested_as_data_series
data_series_nested_as_data_series_bfill = flat_dict_nested_as_data_series_bfill

# nested data series with (backward fill)
data_series_bfill_nested = {
    ('', '', '', 0): 'v0',
    ('', '', 1, 0): 1,
    ('', '', 2, 'l1k0'): 'v2',
    (3, 0, 0, 0): [
        {
            ('', '', '', 0): 'v0',
            ('', '', 1, 0): 1,
            ('', '', 2, 'l1k0'): 'v2',
            (3, 0, 0, 0): [
                {
                    ('', '', '', 0): 'v0',
                    ('', '', 1, 0): 1,
                    ('', '', 2, 'l1k0'): 'v2',
                    (3, 0, 0, 0): [],
                }
            ],
        }
    ],
}
data_series_bfill_nested_as_nested_json = flat_dict_nested_as_nested_json
data_series_bfill_nested_as_flat_dict = flat_dict_nested_as_flat_dict
data_series_bfill_nested_as_data_series = flat_dict_nested_as_data_series
data_series_bfill_nested_as_data_series_bfill = flat_dict_nested_as_data_series_bfill

class TestTransform(unittest.TestCase):
    """
    This class provides test case for transforming data.
    """

    # test nested list transformations
    def test_list_nested_to_flat_dict(self):
        self.assertEqual(
            NestedJson(list_nested).flat_dict,
            list_nested_as_flat_dict
        )

    def test_list_nested_to_data_series(self):
        self.assertEqual(
            NestedJson(list_nested).data_series,
            list_nested_as_data_series
        )

    def test_list_nested_to_data_series_bfill(self):
        self.assertEqual(
            NestedJson(list_nested).data_series_bfill,
            list_nested_as_data_series_bfill
        )

    # test nested dictionary transformations
    def test_dict_nested_to_flat_dict(self):
        self.assertEqual(
            NestedJson(dict_nested).flat_dict,
            dict_nested_as_flat_dict
        )

    def test_dict_nested_to_data_series(self):
        self.assertEqual(
            NestedJson(dict_nested).data_series,
            dict_nested_as_data_series
        )

    def test_dict_nested_to_data_series_bfill(self):
        self.assertEqual(
            NestedJson(dict_nested).data_series_bfill,
            dict_nested_as_data_series_bfill
        )

    # test flat-dict with nested flat-dict
    def test_flat_dict_nested_to_nested_json(self):
        self.assertEqual(
            NestedJson(flat_dict_nested).parsed.nfd.data,
            flat_dict_nested_as_nested_json
        )

    def test_flat_dict_nested_to_flat_dict(self):
        self.assertEqual(
            NestedJson(flat_dict_nested).parsed.nfd.flat_dict,
            flat_dict_nested_as_flat_dict
        )

    def test_flat_dict_nested_to_data_series(self):
        self.assertEqual(
            NestedJson(flat_dict_nested).parsed.nfd.data_series,
            flat_dict_nested_as_data_series
        )

    def test_flat_dict_nested_to_data_series_bfill(self):
        self.assertEqual(
            NestedJson(flat_dict_nested).parsed.nfd.data_series_bfill,
            flat_dict_nested_as_data_series_bfill
        )

    # test data-series with nested data-series
    def test_data_series_nested_to_nested_json(self):
        self.assertEqual(
            NestedJson(data_series_nested).parsed.nds.data,
            data_series_nested_as_nested_json
        )

    def test_data_series_nested_to_flat_dict(self):
        self.assertEqual(
            NestedJson(data_series_nested).parsed.nds.flat_dict,
            data_series_nested_as_flat_dict
        )

    def test_data_series_nested_to_data_series(self):
        self.assertEqual(
            NestedJson(data_series_nested).parsed.nds.data_series,
            data_series_nested_as_data_series
        )

    def test_data_series_nested_to_data_series_bfill(self):
        self.assertEqual(
            NestedJson(data_series_nested).parsed.nds.data_series_bfill,
            data_series_nested_as_data_series_bfill
        )

    # test backfilled data-series with nested backfilled data-series
    def test_data_series_bfill_nested_to_nested_json(self):
        self.assertEqual(
            NestedJson(data_series_bfill_nested).parsed.nds.data,
            data_series_bfill_nested_as_nested_json
        )

    def test_data_series_bfill_nested_to_flat_dict(self):
        self.assertEqual(
            NestedJson(data_series_bfill_nested).parsed.nds.flat_dict,
            data_series_bfill_nested_as_flat_dict
        )

    def test_data_series_bfill_nested_to_data_series(self):
        self.assertEqual(
            NestedJson(data_series_bfill_nested).parsed.nds.data_series,
            data_series_bfill_nested_as_data_series
        )

    def test_data_series_bfill_nested_to_data_series_bfill(self):
        self.assertEqual(
            NestedJson(data_series_bfill_nested).parsed.nds.data_series_bfill,
            data_series_bfill_nested_as_data_series_bfill
        )

if __name__ == '__main__':
    unittest.main()
