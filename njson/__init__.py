from .njson import (
	NestedJson,
	read_json,
	read_json_str,
	from_flat_dict,
	from_nested_flat_dict,
	from_series, 
	from_nested_series
)
__version__ = '1.2.0'
